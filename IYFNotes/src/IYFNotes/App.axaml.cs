using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Avalonia.Platform;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;

namespace IYFNotes
{
    public class App : Application
    {
        private System.Windows.Forms.NotifyIcon _notifyIcon;
        private bool _isExit;

        private Window? _mainWindow;

        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                _mainWindow = desktop.MainWindow = new MainWindow();
                _mainWindow.Closing += MainWindowClosing;
            }

            // Setup a notification icon
            _notifyIcon = new System.Windows.Forms.NotifyIcon();
            _notifyIcon.DoubleClick += (s, args) => ShowMainWindow();

            using (Stream s = AvaloniaLocator.Current.GetService<IAssetLoader>().Open(new Uri("avares://IYFNotes/Assets/notes-main-icon.png")))
            {
                var bitmap = new Bitmap(s);
                var iconHandle = bitmap.GetHicon();
                _notifyIcon.Icon = System.Drawing.Icon.FromHandle(iconHandle);
            }

            _notifyIcon.Visible = true;

            CreateContextMenu();

            base.OnFrameworkInitializationCompleted();
        }

        private void CreateContextMenu()
        {
            _notifyIcon.ContextMenuStrip = new System.Windows.Forms.ContextMenuStrip();
            _notifyIcon.ContextMenuStrip.Items.Add("MainWindow...").Click += (s, e) => ShowMainWindow();
            _notifyIcon.ContextMenuStrip.Items.Add("Exit").Click += (s, e) => ExitApplication();
        }

        private void ExitApplication()
        {
            _isExit = true;
            _mainWindow.Close();
            _notifyIcon.Dispose();
            _notifyIcon = null;
        }

        private void ShowMainWindow()
        {
            if (_mainWindow.IsVisible)
            {
                if (_mainWindow.WindowState == WindowState.Minimized)
                {
                    _mainWindow.WindowState = WindowState.Normal;
                }
                _mainWindow.Activate();
            }
            else
            {
                _mainWindow.Show();
            }
        }

        private void MainWindowClosing(object sender, CancelEventArgs e)
        {
            if (!_isExit)
            {
                e.Cancel = true;
                _mainWindow.Hide();
            }
        }
    }
}
